# BlockChain test

Current repository implementation blockchain

### Quick start
```bash
    pip install setup.py
```


#### Example

```python
    from blockchain.transaction2 import Transaction, build_signature
    from blockchain.block import Block
    from blockchain.blockchain import BlockChain


    transaction = Transaction()
    transaction.set_id(1)
    transaction.setType(Transaction.EMISSION)
    transaction.setTo('bob')
    transaction.set_amount(200)
    transaction.set_signature(build_signature(transaction))
    block = Block()
    block.setId(1)
    block.addTransaction(transaction)
    blockChain = BlockChain()
    blockChain.addBlock(None, block)

    transaction = Transaction()
    transaction.set_id(1)
    transaction.setType(Transaction.TRANSFER)
    transaction.setFrom('bob')
    transaction.setTo('alice')
    transaction.set_amount(50)
    transaction.set_signature(build_signature(transaction))
    
    block = Block()
    block.setId(2)
    block.addTransaction(transaction)
    blockChain.addBlock(1, block)

    transaction = Transaction()
    transaction.set_id(1)
    transaction.setType(Transaction.TRANSFER)
    transaction.setFrom('bob')
    transaction.setTo('alice')
    transaction.set_amount(100)
    transaction.set_signature(build_signature(transaction))

    block = Block()
    block.setId(3)
    block.addTransaction(transaction)
    blockChain.addBlock(2, block)

    transaction = Transaction()
    transaction.set_id(1)
    transaction.setType(Transaction.EMISSION)
    transaction.setTo('alice')
    transaction.set_amount(100)
    transaction.set_signature(build_signature(transaction))

    print('alice', blockChain.get_balance('alice'))
    print('bob', blockChain.get_balance('bob'))
```
