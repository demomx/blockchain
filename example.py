from blockchain.transactions.transaction import Transaction
from blockchain.transactions.enums import TransactionTypes
from blockchain.transactions.helpers import build_signature
from blockchain.chain.block import Block
from blockchain.chain.chain import Chain as BlockChain

block_chain = BlockChain()


def main():
    transaction = Transaction()
    transaction.set_id(1)
    transaction.set_type(TransactionTypes.EMISSION)
    transaction.set_to_account('bob')
    transaction.set_amount(100)
    transaction.set_signature(build_signature(transaction))

    block = Block()
    block.set_id(1)
    block.add_transaction(transaction)

    block_chain.add_block(None, block)

    transaction = Transaction()
    transaction.set_id(2)
    transaction.set_type(TransactionTypes.TRANSFER)
    transaction.set_from_account('bob')
    transaction.set_to_account('alice')
    transaction.set_amount(1)
    transaction.set_signature(build_signature(transaction))

    block = Block()
    block.set_id(2)
    block.add_transaction(transaction)

    try:
        block_chain.add_block(1, block)
    except Exception as e:
        print(e)

    # print(blockChain.getBlockChain())
    # print('alice', blockChain.getBalance('alice'))
    # print('bob', blockChain.getBalance('bob'))
    # block = Block(1)
    # for i in range(10):
    #     transaction = Transaction()
    #     transaction.set_id(i)
    #     transaction.set_type(TransactionTypes.EMISSION)
    #     transaction.set_to_account('bob')
    #     transaction.set_amount(-200)
    #     transaction.set_signature(build_signature(transaction))
    #     block.add_transaction(transaction)
    # # print(block)
    # block_chain.add_block(None, block)
    # transaction = Transaction()
    # transaction.set_id(1)
    # transaction.set_type(TransactionTypes.EMISSION)
    # transaction.set_to_account('bob')
    # transaction.set_amount(200)
    # transaction.set_signature(build_signature(transaction))
    # block = Block()
    # block.set_id(1)
    # block.add_transaction(transaction)
    # block_chain = BlockChain()
    # block_chain.add_block(None, block)
    #
    # print(block_chain.balances)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.TRANSFER)
    # transaction2.set_from_account('bob')
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(50)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block2 = Block()
    # block2.set_id(2)
    # block2.add_transaction(transaction2)
    # block_chain.add_block(1, block2)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.TRANSFER)
    # transaction2.set_from_account('bob')
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block2 = Block()
    # block2.set_id(3)
    # block2.add_transaction(transaction2)
    # block_chain.add_block(2, block2)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(4)
    # block.add_transaction(transaction2)
    # block_chain.add_block(1, block)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(5)
    # block.add_transaction(transaction2)
    # block_chain.add_block(4, block)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(6)
    # block.add_transaction(transaction2)
    # block_chain.add_block(4, block)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(7)
    # block.add_transaction(transaction2)
    # block_chain.add_block(3, block)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(8)
    # block.add_transaction(transaction2)
    # block_chain.add_block(7, block)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(9)
    # block.add_transaction(transaction2)
    # block_chain.add_block(2, block)
    #
    # transaction2 = Transaction()
    # transaction2.set_id(1)
    # transaction2.set_type(TransactionTypes.EMISSION)
    # transaction2.set_to_account('alice')
    # transaction2.set_amount(100)
    # transaction2.set_signature(build_signature(transaction2))
    #
    # block = Block()
    # block.set_id(10)
    # block.add_transaction(transaction2)
    # block_chain.add_block(9, block)

    print(block_chain.get_block_chain())
    print('alice', block_chain.get_balance('alice'))
    print('bob', block_chain.get_balance('bob'))


if __name__ == '__main__':
    main()
