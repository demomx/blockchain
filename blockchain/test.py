import unittest
from blockchain.transactions.transaction import Transaction


class TestTransaction(unittest.TestCase):

    def test_create(self):
        transaction = Transaction()
        transaction.set_id(1)
        self.assertEqual(transaction.id, 1)

if __name__ == '__main__':
    unittest.main()