from typing import List
from blockchain.chain.block import Block
from blockchain.tree.exceptions import TreeNodeInitialException, TreeNodeAddNode


class TreeNode:
    """Class from create node in tree
    """
    __children__: List = None
    __children_count__: int = None
    __parent_id__: int = None
    __id__: int = 0
    __data__: Block = None

    def __init__(self, item_id: int, data: Block, parent_id: int):

        self.__id__ = item_id
        self.__parent_id__ = parent_id
        self.__children__ = []
        self.__children_count__ = len(self.__children__)

        if data is None or not isinstance(data, Block):
            raise TreeNodeInitialException(f"Incorrect type data {repr(data)}, must be type Block")
        else:
            self.__data__ = data

    def __repr__(self) -> str:
        """Method from get string current node
            Returns:
                str
        """
        return f"<{self.__class__.__name__} children={repr(self.children)} item_id={repr(self.__id__)}>"

    def add_child(self, child_node):
        """Method from add child node
            Args:
                child_node (TreeNode): add child node
            Returns:
                void
        """
        if not isinstance(child_node, TreeNode):
            raise TreeNodeAddNode(f"Incorrect type data {repr(child_node)}, must be type BlockChainTreeNode")
        self.__children_count__ += 1
        self.__children__.append(child_node)

    @property
    def data(self) -> Block:
        """Method from get data current node
            Returns:
                data (Block)
        """
        return self.__data__

    @property
    def id(self) -> int:
        """Method from get id current node
            Returns:
                id (int): id current node
        """
        return self.__id__

    @property
    def len_children(self) -> int:
        return self.__children_count__

    @property
    def children(self) -> List:
        """Method from get children current node
            Returns:
                children (list): children current node
        """
        return self.__children__
