from blockchain.transactions.transaction import Transaction
from typing import Any, List
from blockchain.tree.node import TreeNode
from blockchain.chain.block import Block
from blockchain.tree.exceptions import TreeAddBlock, TreeNotFoundNode, TreeUndefinedRootNode


class Tree:
    """Class from tree structure
    """
    __root__ = None
    __index_ids__ = []
    __balance_cache__ = {}

    def __init__(self):
        super().__init__()

    @property
    def root(self):
        """Method from get root node by tree
            Returns:
                node(BlockChainTreeNode)
        """
        return self.__root__

    def is_exist_id(self, item_id: int) -> bool:
        """Method from check exist id in tree
            Returns:
                true or false (bool)
        """
        return item_id in self.__index_ids__

    def append(self, parent_id: int, data: Block = None) -> TreeNode:
        """Method from add node by tree
            Args:
                parent_id (int): parent_id node
                data (Block): data to add in node
            Returns:
                node(TreeNode)
        """
        if not isinstance(data, Block):
            raise TreeAddBlock(f"Incorrect type for data must be Block")

        node = TreeNode(data.id, data, 0)

        if parent_id is None or parent_id == 0:
            if self.__root__ is None:
                self.__root__ = node
                self.__index_ids__.append(data.id)
                return node

        self.search(parent_id).add_child(node)
        self.__index_ids__.append(data.id)
        return node

    def max_chain(self) -> List[TreeNode]:
        """Method from get max chain by tree
            Returns:
                list nodes
        """
        if self.root is None:
            return []

        return_path = [self.root]
        max_path = []
        for node in self.root.children:
            path = self.get_longest_path_length(node)
            if len(max_path) < len(path):
                max_path = path
        return_path = return_path + max_path
        return return_path

    def get_longest_path_length(self, node: TreeNode) -> List[TreeNode]:
        """Method from get path by parent node
            Args;
                node (TreeNode): parent node
            Returns:
                list nodes
        """
        if node.len_children == 0:
            return []
        else:
            result = [node]
            for node_child in node.children:
                result = result + self.get_longest_path_length(node_child)
            return result

    def search(self, node_id: int) -> TreeNode:
        """Method from search node by node_id
            Args;
                node_id (int): find node by id
            Returns:
                node (TreeNode) or None: node
        """
        if self.__root__ is None:
            raise TreeUndefinedRootNode("")

        if self.__root__.id == node_id:
            return self.__root__

        for node in self.to_rows(self.__root__.children):
            if node.id == node_id:
                return node

        raise TreeNotFoundNode(f"with id {node_id}")

    def to_rows(self, arr: List[TreeNode]) -> List[TreeNode]:
        """Method from get tree to rows
            Args;
                arr (list): get
            Returns:
                node (TreeNode): node
        """
        result = []
        for node in arr:
            if isinstance(node, TreeNode):
                result = result + [node]
                if node.len_children:
                    result = result + self.to_rows(node.children)
        return result
