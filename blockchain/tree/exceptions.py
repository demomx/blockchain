class TreeNodeInitialException(Exception):
    def __init__(self, message):
        super().__init__(f"Initial TreeNode exception: {message}")


class TreeNodeAddNode(Exception):
    def __init__(self, message):
        super().__init__(f"Add new Node to TreeNode exception: {message}")


class TreeAddBlock(Exception):
    def __init__(self, message):
        super().__init__(f"Add new Block to Tree exception: {message}")


class TreeNotFoundNode(Exception):
    def __init__(self, message):
        super().__init__(f"Tree not found node exception: {message}")


class TreeUndefinedRootNode(Exception):
    def __init__(self, message):
        super().__init__(f"Tree not found root node exception: {message}")
