from typing import List
from blockchain.transactions.transaction import Transaction
from blockchain.transactions.helpers import build_signature
from blockchain.chain.exceptions import BlockAddTransactionException


class Block:
    """
        Basic class defined Block type in chain
    """
    __id__: int = None
    __transactions__: List = None
    __transactions_ids__: List = None

    def __init__(self, block_id: int = 0, transactions: List[Transaction] = None):
        """Construct method
            Args:
                block_id (int): set id from block
                transactions (list): add transaction to current block
            Returns:
                void
        """
        super().__init__()
        self.__transactions__ = []
        self.__transactions_ids__ = []
        self.__id__ = block_id
        if transactions is not None and len(transactions) > 0:
            for transaction in transactions:
                self.add_transaction(transaction)

    @property
    def id(self) -> int:
        """Method from get id current block
            Returns:
                id (int): id current block
        """
        return self.__id__

    def set_id(self, _id_: int):
        """Method from set id current block
            Args:
                _id_ (int): set id from current block
            Returns:
                void
        """
        self.__id__ = _id_

    @property
    def transactions(self) -> List[Transaction]:
        """Method from get list transaction in block
            Returns:
                transactions (list): list transaction element type
                                        Transaction class
        """
        return self.__transactions__

    @staticmethod
    def validate_transaction(transaction: Transaction) -> bool:
        """Method from validate sign in transaction
            Args:
                transaction (Transaction): transaction object from validate
            Returns:
                true or false
        """
        return transaction.signature == build_signature(transaction)

    def add_transaction(self, transaction: Transaction):
        """Method from add transaction to block
            Args:
                transaction (Transaction): transaction from add to block
            Returns:
                void
        """
        if not self.validate_transaction(transaction):
            raise BlockAddTransactionException('Incorrect transaction')

        if len(self.__transactions__) + 1 > 10:
            raise BlockAddTransactionException('Max length transaction current block')

        if transaction.id in self.__transactions_ids__:
            raise BlockAddTransactionException(
                f"Transaction already exist {transaction.id} in {self.__transactions_ids__}")

        self.__transactions__.append(transaction)
        self.__transactions_ids__.append(transaction.id)
