from typing import Dict
from blockchain.transactions.transaction import Transaction
from blockchain.transactions.enums import TransactionTypes
from blockchain.chain.exceptions import BalanceNotMoney, BalanceUndefinedTypeTransaction, BalanceUndefinedUser, \
    BalanceStateException


class BalanceCache:
    __users__: Dict = None

    def __init__(self):
        super().__init__()
        self.__users__ = {}

    @property
    def users(self) -> Dict:
        return self.__users__

    def transfer(self, transaction: Transaction):
        to_account = transaction.to_account
        from_account = transaction.from_account
        self.init_account_if_not_exist(to_account)
        self.init_account_if_not_exist(from_account)

        if transaction.type == TransactionTypes.EMISSION:
            self.add_balance(to_account, transaction.amount)
        elif transaction.type == TransactionTypes.TRANSFER:
            self.minus_balance(from_account, transaction.amount)
            self.add_balance(to_account, transaction.amount)
        else:
            raise BalanceUndefinedTypeTransaction(transaction.type)

    def init_account_if_not_exist(self, account_name: str):
        if account_name is not None and not self.is_exist_user(account_name):
            self.init_account(account_name)

    def is_exist_user(self, account_name: str) -> bool:
        return account_name in self.__users__

    def init_account(self, account_name: str):
        self.__users__[account_name] = {
            "balance": 0,
            "block": False
        }

    def is_correct_state(self):
        for account in self.__users__:
            if self.get_balance(account) < 0:
                raise BalanceStateException(f"Operation add negative balance {account}")

    def get_balance(self, account_name: str) -> int:
        if not self.is_exist_user(account_name):
            raise BalanceUndefinedUser(account_name)
        return self.__users__[account_name]["balance"]

    def add_balance(self, account_name: str, price: int) -> int:
        if not self.is_exist_user(account_name):
            raise BalanceUndefinedUser(account_name)
        self.__users__[account_name]["balance"] += price
        return self.__users__[account_name]["balance"]

    def minus_balance(self, account_name: str, price: int) -> int:
        if not self.is_exist_user(account_name):
            raise BalanceUndefinedUser(account_name)

        if self.get_balance(account_name) == 0:
            raise BalanceNotMoney(self.get_balance(account_name), price, account_name)

        if (self.get_balance(account_name) - price) < 0:
            raise BalanceNotMoney(self.get_balance(account_name), price, account_name)

        self.__users__[account_name]["balance"] -= price

        return self.__users__[account_name]["balance"]

    def clear(self):
        self.__users__ = {}
