class BlockAddTransactionException(Exception):
    def __init__(self, message):
        super().__init__(f"Block add transaction exception: {message}")


class BalanceNotMoney(Exception):
    def __init__(self, user_money: int, transaction_money: int, user: str):
        super().__init__(f"Balance not money:{user} have in balance {user_money},"
                         f" but transaction need {transaction_money}")


class BalanceUndefinedTypeTransaction(Exception):
    def __init__(self, transaction_type: str):
        super().__init__(f"Balance undefined type transaction: {transaction_type}")


class BalanceUndefinedUser(Exception):
    def __init__(self, user_name: str):
        super().__init__(f"Balance undefined user: {user_name}")


class BalanceStateException(Exception):
    def __init__(self, message: str):
        super().__init__(f"Balance state incorrect: {message}")
