from typing import Dict
from blockchain.tree.tree import Tree
from blockchain.chain.block import Block
from blockchain.transactions.transaction import Transaction
from blockchain.chain.exceptions import BalanceUndefinedUser
from blockchain.transactions.enums import TransactionTypes
from blockchain.chain.exceptions import BalanceNotMoney
from blockchain.chain.balance_cache import BalanceCache


class Chain(object):
    """Class from create chain for blocks
    """
    __tree__: Tree = None
    __balance__: BalanceCache = None

    def __init__(self):
        super().__init__()
        self.__tree__ = Tree()
        self.__balance__ = BalanceCache()

    def add_block(self, parent_id: int, block: Block):
        """Method from add block to chain tree
            Args:
                parent_id (int): parent id block
                block (Block): block
            Returns:
                void
        """
        if self.validate_block(block):

            for transaction in block.transactions:
                self.__balance__.transfer(transaction)

            self.__balance__.is_correct_state()

            if parent_id is None:
                if self.__tree__.root is None:
                    # self.__balance__.clear()
                    self.__tree__.append(None, block)
            else:
                # self.__balance__.clear()
                self.__tree__.append(parent_id, block)
        else:
            raise Exception('Node with id {} exist'.format(block.id))

    def get_block_chain(self):
        """Method return list nodes without last child
            Returns:
                list nodes
        """
        return self.__tree__.max_chain()

    def validate_block(self, block):
        """Method validate block
            Args:
                block (Block): block from validate
            Returns:
                true or false (bool)
        """
        if not isinstance(block, Block):
            return False

        if len(block.transactions) == 0:
            return False

        if self.__tree__.is_exist_id(block.id):
            return False

        return True

    def get_balance(self, account):
        """Method get balance from account
            Args:
                account (str): account from length 2 < account < 10
            Returns:
                balance (int)
        """
        Transaction.check_account_name(account)
        try:
            return self.__balance__.get_balance(account)
        except BalanceUndefinedUser:
            return 0

    @property
    def balances(self) -> Dict:
        return self.__balance__.users
    # def __clearBalanceCache(self):
    #     """Method from clear cache balance
    #         Returns:
    #             void
    #     """
    #     self.balance_cache = {}

    # def __getBalanceCache(self):
    #     """Method from get balance cahce
    #         Returns:
    #             void
    #     """
    #     self.__clearBalanceCache()
    #     if self.__tree__.root is not None:
    #         for transaction in self.__tree__.root.data.transaction:
    #             self.__balance__.transfer(transaction)
    #             # self.__calculate_balance__(transaction)
    #
    #         for node in self.__tree__.to_rows(self.__tree__.root.children):
    #             for transaction in node.data.transactions:
    #                 self.__balance__.transfer(transaction)
    #                 # self.__calculate_balance__(transaction)

    # def __calculate_balance__(self, transaction):
    #     """Method from calculate balance
    #         Args:
    #             transaction (Transaction): transaction from calculate balance
    #         Returns:
    #             void
    #     """
    #
    #     if transaction.to_account is not None and transaction.to_account not in self.balance_cache:
    #         self.balance_cache[transaction.to_account] = 0
    #
    #     if transaction.from_account is not None and transaction.from_account not in self.balance_cache:
    #         self.balance_cache[transaction.from_account] = 0
    #
    #     if transaction.type == TransactionTypes.EMISSION:
    #         self.balance_cache[transaction.to_account] = self.balance_cache[transaction.to_account] + transaction.amount
    #
    #     if transaction.type == TransactionTypes.TRANSFER:
    #         if self.balance_cache[transaction.from_account] < 0 or (self.balance_cache[
    #                                                                     transaction.from_account] - transaction.amount) < 0:
    #             raise BalanceNotMoney(self.balance_cache[transaction.from_account], transaction.amount,
    #                                   transaction.from_account)
    #
    #         self.balance_cache[transaction.to_account] = self.balance_cache[transaction.to_account] + transaction.amount
    #         self.balance_cache[transaction.from_account] = self.balance_cache[
    #                                                            transaction.from_account] - transaction.amount
