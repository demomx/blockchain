class TransactionNameException(Exception):
    def __init__(self, message):
        super().__init__(f"Transaction name exception: {message}")


class TransactionTypeException(Exception):
    def __init__(self, message):
        super().__init__(f"Transaction type exception: {message}")


class TransactionAmountException(Exception):
    def __init__(self, message):
        super().__init__(f"Transaction amount exception: {message}")


class TransactionSignatureException(Exception):
    def __init__(self, message):
        super().__init__(f"Transaction signature exception: {message}")


class TransactionAccountException(Exception):
    def __init__(self, message):
        super().__init__(f"Transaction account exception: {message}")


class BuildSignatureException(Exception):
    def __init__(self, message):
        super().__init__(f"Transaction signature exception: {message}")
