from enum import Enum


class TransactionTypes(Enum):
    EMISSION = 0
    TRANSFER = 1
