import hashlib
from blockchain.transactions.transaction import Transaction
from blockchain.transactions.exceptions import BuildSignatureException


def build_signature(t):
    """Function from generate signature
        Args:
            t (Transaction): generate signature from transaction
        Returns:
            sign (str)
    """
    if not isinstance(t, Transaction):
        raise BuildSignatureException('Incorrect class object. I need object from class Transaction')
    sign = hashlib.md5()
    sign.update(f"{t.id}:{t.type}:{t.from_account}:{t.to_account}:{t.amount}".encode("utf-8"))
    return str(sign.hexdigest())
