from enum import Enum
from blockchain.transactions.enums import TransactionTypes
from blockchain.transactions.exceptions import TransactionNameException, TransactionTypeException, \
    TransactionAmountException, TransactionSignatureException, TransactionAccountException


class Transaction(object):
    """Transaction class to Blockchain
    """

    def __init__(self, _id=None, _type=None, _from=None, _to=None, _amount=None, _signature=None):
        super(Transaction, self).__init__()
        self.__id = _id
        self.__type = _type
        self.__from = _from
        self.__to = _to
        self.__amount = _amount
        self.__signature = _signature

    @property
    def id(self):
        """Method from get id current transaction
            Returns:
                id (int): id current transaction
        """
        return self.__id

    def set_id(self, _id_: int):
        """Method from set id current transaction
            Args:
                _id_ (int): id from current transaction
            Returns:
                void
        """
        self.__id = _id_

    @property
    def type(self):
        """Method from get type current transaction
            Returns:
                type (int): type current transaction
        """
        return self.__type

    @staticmethod
    def check_type(_type_: Enum):
        """Method from check type current transaction
            Private method
            Args:
                _type_ (int): id from current transaction
            Returns:
                true or false (bool)
        """
        if _type_ is None:
            raise TransactionTypeException("type is None")

        if _type_ not in [TransactionTypes.EMISSION, TransactionTypes.TRANSFER]:
            raise TransactionTypeException(f"type don't exist in range")

    def set_type(self, _type_: Enum):
        """Method from set _type_ current transaction
            Args:
                _type_ (int): type from current transaction (EMISSION or TRANSFER)
            Returns:
                void
        """
        if self.check_type(_type_):
            raise TransactionTypeException(f"Incorrect type transaction. Type is {_type_}")
        if _type_ == TransactionTypes.EMISSION:
            self.__from = None

        self.__type = _type_

    @property
    def from_account(self):
        """Method from get from account current transaction
            Returns:
                from (str): from current transaction
        """
        return self.__from

    def set_from_account(self, _from_):
        """Method from set from current transaction
            Args:
                _from_ (str): account from current transaction
            Returns:
                void
        """
        self.check_type(self.__type)

        if not self.__type == TransactionTypes.EMISSION:
            self.check_account_name(_from_)
            if self.__to is not None:
                if self.__to == _from_:
                    raise TransactionAccountException(f"to account equal from account ({self.__to} == {_from_})")
            self.__from = _from_
        else:
            self.__from = None

    @property
    def to_account(self):
        """Method from get to account current transaction
            Returns:
                to (str): to current transaction
        """
        return self.__to

    def set_to_account(self, _to_):
        """Method from set to current transaction
            Args:
                _to_ (str): account from current transaction
            Returns:
                void
        """
        self.check_account_name(_to_)
        if self.__from is not None:
            if self.__from == _to_:
                raise TransactionAccountException(f"from account equal to account ({self.__from} == {_to_})")
        self.__to = _to_

    @property
    def amount(self):
        """Method from get amount current transaction
            Returns:
                amount (int): amount current transaction
        """
        return self.__amount

    def set_amount(self, _amount_):
        """Method from set amount current transaction
            Args:
                _amount_ (int): amount from current transaction
            Returns:
                void
        """
        if _amount_ == 0:
            raise TransactionAmountException('Amount of the value is zero')

        if _amount_ < 0:
            raise TransactionAmountException("Amount can't be with minus")

        self.__amount = _amount_

    @property
    def signature(self):
        """Method from get sign current transaction
            Returns:
                sign (str): sign current transaction
        """
        return self.__signature

    def set_signature(self, _signature_):
        """Method from set signature current transaction
            Args:
                _signature_ (str): signature from current transaction
            Returns:
                void
        """
        if len(_signature_) != 32:
            raise TransactionSignatureException('Signature length greater than 32 symbols')
        self.__signature = _signature_

    @staticmethod
    def check_account_name(name):
        """Method from check name current transaction
            Static method
            Args:
                name (str): id from current transaction
            Returns:
                true or false (bool)
                message if incorrect value
        """
        if name is None:
            raise TransactionNameException("Name is None")

        if 10 < len(name) < 2:
            raise TransactionNameException(f"Incorrect length name must be to range at 2 to 10,"
                                           f" but length equals {len(name)}")
