from setuptools import setup, find_packages

__name__ = 'blockchain'
__version__ = '0.0.1'

__author__ = 'Vladimir Goncharov'
__author_email__ = 'demomx1998@gmail.com'

maintainers = ()

description = '''
'''

package_params = {
    'name': __name__,
    'version': __version__,
    'packages': find_packages('.'),
    'url': '',
    'license': '',
    'author': __author__,
    'author_email': __author_email__,
    'include_package_data': True,
    'install_requires': [
    ],
    'description': description
}

# package_params.update(**maintainers[-1])

setup(**package_params)
